#myapp/telegrambot.py
# Example code for telegrambot.py module
import datetime
import json

from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render_to_response
from telegram.ext import CommandHandler, MessageHandler, Filters
from django_telegrambot.apps import DjangoTelegramBot

import logging

from bot.models import MessageFromBot

logger = logging.getLogger(__name__)

CHANNEL_NAME = '@testtesttest1234'
# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')


def help(bot, update):
    bot.sendMessage(update.message.chat_id, text='Help!')


def echo(bot, update):
    bot.sendMessage(update.message.chat_id, text=update.message.text)
    bot.sendMessage(update.message.chat_id, text=update.message.text + "dddd")
    bot.sendMessage(CHANNEL_NAME, "something wrong3")
    print('**')
    all_messages = MessageFromBot.objects.all()

    # print('**5')
    json1 = serializers.serialize('json', all_messages)
    # print('*3*')
    json2 = json.loads(json1)
    print(json2)
    # print(json2)

    while True:
        for i in json2:
            if (abs(int(i['fields']['epoch']) - int(datetime.datetime.now().timestamp())) < 20):
                print(abs(int(i['fields']['epoch'])))
                print(int(datetime.datetime.now().timestamp()))
                r = json2.pop(json2.index(i))
                bot.sendMessage(CHANNEL_NAME, r['fields']['title'])
                continue

def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    logger.info("Loading handlers for telegram bot")
    # Default dispatcher (this is related to the first bot in settings.TELEGRAM_BOT_TOKENS)
    dp = DjangoTelegramBot.dispatcher
    # To get Dispatcher related to a specific bot
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_token')     #get by bot token
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_username')  #get by bot username

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([Filters.text], echo))

    # log all errors
    dp.add_error_handler(error)

    # log all errors
    dp.addErrorHandler(error)


