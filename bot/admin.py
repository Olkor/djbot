from django.contrib import admin

# Register your models here.
from .models import MessageFromBot

admin.site.register(MessageFromBot)