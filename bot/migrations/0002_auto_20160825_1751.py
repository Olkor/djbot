# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='messagefrombot',
            name='epoch',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='messagefrombot',
            name='date',
            field=models.DateTimeField(),
        ),
    ]
