import time
from django.db import models

# Create your models here.
class MessageFromBot(models.Model):
    title = models.CharField(max_length=80)
    text = models.TextField()
    date = models.DateTimeField()

    epoch = models.BigIntegerField(default=0)

    def save(self, *args, **kwargs):
        super(MessageFromBot, self).save(*args, **kwargs)

        for item in MessageFromBot.objects.all():
            if (item.epoch == 0):
                item.epoch = item.date.timestamp()
                item.save()


    def __str__(self):
        return "{}".format(self.title)